<?php

namespace Drupal\ad_entity_adition\Plugin\ad_entity\AdType;

use Drupal\ad_entity\Entity\AdEntityInterface;
use Drupal\ad_entity\Plugin\AdTypeBase;
use Drupal\ad_entity\TargetingCollection;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

/**
 * Type plugin for Smart advertisement.
 *
 * @AdType(
 *   id = "adition",
 *   label = "ADITION technologies AG"
 * )
 */
class AditionType extends AdTypeBase {

  /**
   * {@inheritdoc}
   */
  public function globalSettingsForm(array $form, FormStateInterface $form_state, Config $config) {
    $element = [];

    $settings = $config->get($this->getPluginDefinition()['id']);

    $element['ad_farm'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Adfarm'),
      '#description' => $this->stringTranslation->translate('The adfarm to which your network is assigned is part of all common ADITION website tags. Login into the ADITION frontend, open a contentunit object and select the Website-Tag tab. In the displayed website tag code the src-Attribute of the second script element contains the adfarm which applies to your network.<br/>Do not provide the protocol within the passed adfarm string since the API object will automatically detect the document’s protocol (either “http” or “https”).<br/>If you provide the protocol then the internal protocol detection is overridden. This could lead to security issues if the page is loaded using ssl!'),
      '#default_value' => !empty($settings['ad_farm']) ? $settings['ad_farm'] : '',
      '#size' => 15,
      '#required' => TRUE,
    ];

    $element['universal_tag'] = [
      '#type' => 'checkbox',
      '#title' => $this->stringTranslation->translate('Activate ADITION Universal Tag'),
      '#description' => $this->stringTranslation->translate('ADITION customer can tag and retarget a user on a website on a browser without 3rd party cookies'),
      '#default_value' => !empty($settings['universal_tag']) ? $settings['universal_tag'] : '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function entityConfigForm(array $form, FormStateInterface $form_state, AdEntityInterface $ad_entity) {
    $element = [];

    $settings = $ad_entity->getThirdPartySettings($this->getPluginDefinition()['provider']);

    $element['position_id'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate("Position ID"),
      '#default_value' => !empty($settings['position_id']) ? $settings['position_id'] : '',
      '#size' => 15,
      '#required' => TRUE,
    ];

    $context = !empty($settings['targeting']) ? $settings['targeting'] : [];
    $targeting = isset($context['targeting']) ?
      new TargetingCollection($context['targeting']) : NULL;
    $element['targeting'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate("Default targeting"),
      '#description' => $this->stringTranslation->translate("Default pairs of key-values for targeting on the ad tag. Example: <strong>pos: top, category: value1, category: value2, ...</strong>"),
      '#default_value' => isset($targeting) ? $targeting->toUserOutput() : '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function entityConfigSubmit(array &$form, FormStateInterface $form_state, AdEntityInterface $ad_entity) {
    $provider = $this->getPluginDefinition()['provider'];
    $values = $form_state->getValue(['third_party_settings', $provider]);

    $targeting_empty = TRUE;
    $targeting_value = trim($values['targeting']);
    if (!empty($targeting_value)) {
      // Set the default targeting as context settings.
      $targeting = new TargetingCollection();
      $targeting->collectFromUserInput($targeting_value);
      if (!$targeting->isEmpty()) {
        $context_data = ['targeting' => $targeting->toArray()];
        $ad_entity->setThirdPartySetting($provider, 'targeting', $context_data);
        $targeting_empty = FALSE;
      }
    }
    if ($targeting_empty) {
      $ad_entity->setThirdPartySetting($provider, 'targeting', NULL);
    }
  }

}
