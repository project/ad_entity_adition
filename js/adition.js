/**
 * @file
 * ADITION js helper library.
 */

(function (drupalSettings) {

  /**
   * Smart ad helper class.
   *
   * @constructor
   */
  function Adition() {

    /**
     * Gets domain.
     *
     * @returns {string}
     *   The domain.
     */
    this.getAdFarm = function () {
      return drupalSettings.ad_entity_adition.ad_farm;
    };

    /**
     * Gets non-initialized ad-positions.
     *
     * @returns {Object}
     *   The ad-position DOM-elements.
     */
    this.getNotInitializedPositions = function () {
      return document.querySelectorAll('.ad-entity-container.not-initialized .ad-entity-adition');
    }

    /**
     * Gets adition params.
     *
     * @return {Array}
     */
    this.getAditionParams = function () {
      // Get params from the settings.
      var aditionParams = drupalSettings.ad_entity_adition.aditionParams ?? [];
      if (typeof document.getElementsByTagName('body')[0].dataset.adition !== 'undefined') {
        // Extend with params from the body "data-adition" attribute.
        aditionParams = aditionParams.concat(document.getElementsByTagName('body')[0].dataset.adition.split(':'));
      }
      return aditionParams;
    }
  }

  // Export the class.
  drupalSettings.ad_entity_adition.Adition = Adition;

})(drupalSettings);
