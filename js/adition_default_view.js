/**
 * @file
 * JS View implementation for 'adition_default' ads view plugin.
 */
var adition = adition || {};
adition.srq = adition.srq || [];

// Initialize adition library.
(function () {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = (document.location.protocol === "https:" ? "https:" : "http:") + "//imagesrv.adition.com/js/srp.js";
  script.charset = "utf-8";
  script.async = TRUE;
  var firstScript = document.getElementsByTagName("script")[0];
  firstScript.parentNode.insertBefore(script, firstScript);
})();

(function (Drupal, drupalSettings, window) {
  if (typeof adition == "undefined") {
    return;
  }

  Drupal.ad_entity = Drupal.ad_entity || window.adEntity || {};
  Drupal.ad_entity.viewHandlers = Drupal.ad_entity.viewHandlers || {};
  var handler = new drupalSettings.ad_entity_adition.Adition();

  Drupal.ad_entity.viewHandlers.adition_default = {
    initialize: function (containers, context, settings) {
      var self = this;
      if (containers) {
        adition.srq.push(function (api) {
          self.initAdServer(api);
          self.renderAds(api, containers);
        });
      }
    },

    /**
     * Inits ad server configuration.
     *
     * @param api
     */
    initAdServer: function (api) {
      // Set up the adfarm.
      api.registerAdfarm(handler.getAdFarm());

      // Set adition params.
      var aditionParams = handler.getAditionParams();

      for (var aditionParam in aditionParams){
        if (aditionParams.hasOwnProperty(aditionParam)) {
          api.setProfile(aditionParam, aditionParams[aditionParam]);
        }
      }
    },

    /**
     * Renders ads.
     *
     * @param api
     * @param containers
     */
    renderAds: function (api, containers) {
      var self = this;

      // Render ad positions.
      Object.values(containers).forEach(function (container, i) {
        var adPosition = container.el.querySelector('.ad-entity-adition');
        if (adPosition) {
          self.renderAd(adPosition, api);
        }
      });

      api.load().completeRendering();

      // Register ads pre-render.
      api.events.onPreRender(function (adPosition, bannerDescriptionApi, renderControlApi) {
        self.preRenderAd(adPosition, api, bannerDescriptionApi, renderControlApi);
      });

      // Register ads post-render.
      api.events.onPostRender(function (adPosition, bannerDescriptionApi, renderControlApi) {
        self.postRenderAd(adPosition, api, bannerDescriptionApi, renderControlApi);
      });
    },

    /**
     * Renders ad position.
     *
     * @param adPosition
     * @param api
     */
    renderAd: function (adPosition, api) {
      var name = adPosition.dataset.adPositionName;
      var id = parseInt(adPosition.dataset.adPositionId);
      if (id) {
        api.configureRenderSlot(name).setContentunitId(id);
        api.renderSlot(name);
      }
    },

    /**
     * Pre-renders ads.
     *
     * @param adPosition
     * @param api
     * @param bannerDescriptionApi
     * @param renderControlApi
     */
    preRenderAd: function (adPosition, api, bannerDescriptionApi, renderControlApi) {
      var container = adPosition.closest('.ad-entity-container');
      if (container) {
        // Do not display banner empty banners (option "blocker" is "1").
        if (bannerDescriptionApi.getOptions().blocker === '1') {
          renderControlApi.disable();
          container.style.display = 'none';
        }
        else {
          container.display = 'block';
          container.classList.remove('not-initialized');
          container.classList.add('initialized');
        }
      }
    },

    /**
     * Post-renders ads.
     *
     * @param adPosition
     * @param api
     * @param bannerDescriptionApi
     * @param renderControlApi
     */
    postRenderAd: function (adPosition, api, bannerDescriptionApi, renderControlApi) {
      var container = adPosition.closest('.ad-entity-container');
      var iFrame = adPosition.getElementsByTagName('iframe')[0];

      // Hide empty ads.
      if (!iFrame.contentDocument || bannerDescriptionApi.getOptions().noAd === '1') {
        adPosition.classList.add('no-ad');
        container.style.display = 'none';
      }
    },

    detach: function (containers, context, settings) {},
  };

}(Drupal, drupalSettings, window));
