ADITION AdServer
==============

This module provides integration between the [Advertising Entity](https://www.drupal.org/project/ad_entity) module and [ADITION technologies AG](https://adition.com/).

## Setup

1. Install the module
2. Visit /admin/structure/ad_entity/global-settings
3. Navigate below to the "ADITION technologies AG types" group.
4. Set the ad-farm URL of your ADITION account.
5. Create ad entities at /admin/structure/ad_entity
6. Set the position ids while creating an ad entity, 
   use the default view handler.
7. Create display configuration for your ad entity 
   at /admin/structure/ad_entity/display
8. Display the ad at your page. 
   E.g. you can show it as a block at /admin/structure/block.
